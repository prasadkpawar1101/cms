(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n  <h1>\n\n    <router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component/home/home.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component/home/home.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wcontainer\">\n    <div class=\"whatsapp\">\n        <div class=\"whatsapp_main\">\n            <div class=\"row\">\n                <div class=\"col-lg-4\">\n                    <div class=\"whatsapp_main_sidemenu\">\n\n                        <div class=\"whatsapp_main_sidemenu_middle\">\n                            <i class=\"fa fa-search\"></i>\n                            <input style=\"width:300px\" id=\"searchBox\" placeholder=\"Search\" name=\"search\">\n\n                            <button data-toggle=\"modal\" data-target=\"#addContactModal\" style=\"display: inline;margin-left:5px;border-radius :20px;\">+</button>\n                        </div>\n                        <div class=\"whatapp_main_sidemenu_bottom scroll\">\n                            <div class=\"whatapp_main_sidemenu_bottom_chat\">\n                                <ul id=\"myList\" role=\"button\" style=\"cursor : pointer\" class=\"list-group\">\n                                    <li *ngFor=\"let one of contactList\" class=\"list-group-item contactList\" (click)=\"getUserDetailsById(one.id)\">\n                                        <div class=\"row\">\n                                            <div class=\"col-3 p-0\">\n                                                <img style=\"border-radius : 50px\" [src]=\"one.avatar\" class=\"avatar\" alt=\"\">\n                                            </div>\n                                            <div class=\"col-9\">\n                                                <h3 style=\"text-align:left\" class=\"discription\">\n                                                    {{one.displayName}}\n                                                </h3>\n                                                <p style=\"font-size: 14px;text-align:left\">{{one.countryCode}} {{one.phoneNumber}}\n                                                    <!-- <span><svg id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\"\n                                                        viewBox=\"0 0 18 18\" width=\"18\" height=\"18\">\n                                                        <path fill=\"#4FC3F7\"\n                                                            d=\"M17.394 5.035l-.57-.444a.434.434 0 0 0-.609.076l-6.39 8.198a.38.38 0 0 1-.577.039l-.427-.388a.381.381 0 0 0-.578.038l-.451.576a.497.497 0 0 0 .043.645l1.575 1.51a.38.38 0 0 0 .577-.039l7.483-9.602a.436.436 0 0 0-.076-.609zm-4.892 0l-.57-.444a.434.434 0 0 0-.609.076l-6.39 8.198a.38.38 0 0 1-.577.039l-2.614-2.556a.435.435 0 0 0-.614.007l-.505.516a.435.435 0 0 0 .007.614l3.887 3.8a.38.38 0 0 0 .577-.039l7.483-9.602a.435.435 0 0 0-.075-.609z\">\n                                                        </path>\n                                                    </svg></span> Message -->\n                                                </p>\n                                            </div>\n                                        </div>\n\n                                    </li>\n\n\n\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-lg-8 col-xs-0 pr-0 pl-0 m-0\">\n\n                    <div class=\"whatsapp_main_body p-5\">\n\n                        <div *ngIf=\"!userData.avatar\" class=\"whatsapp_main_body_image\">\n                            <img src=\"https://cdn2.iconfinder.com/data/icons/flat-style-svg-icons-part-1/512/address_book-512.png\" alt=\"\">\n                        </div>\n                        <div *ngIf=\"userData.avatar\" class=\"container\">\n                            <div class=\".col-xs-4 .col-md-offset-2\">\n                                <div class=\"panel panel-default panel-info Profile\">\n\n\n                                    <div class=\"panel-body\">\n                                        <div>\n                                            <div align=\"center\" class=\"centered\">\n                                                <div class=\"hovereffect mb-5 row\">\n                                                    <img *ngIf=\"!userData.avatar\" style=\"height:250px;width:250px;border-radius :20px;\" src=\"https://web.whatsapp.com/img/c98cc75f2aa905314d74375a975d2cf2.jpg\">\n                                                    <img *ngIf=\"userData.avatar\" style=\"height:250px;width:250px;border-radius :20px;\" [src]=\"userData.avatar\">\n                                                    <div class=\"overlay\">\n\n                                                        <a class=\"info\" data-toggle=\"modal\" data-target=\"#addProfileModal\">Edit</a>\n                                                    </div>\n                                                </div>\n                                            </div>\n\n\n                                            <br>\n                                            <br>\n                                            <br>\n                                            <br>\n                                            <br>\n                                            <br>\n                                            <br>\n\n                                            <form>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Display Name</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"displayName\" placeholder=\"Display name\" [(ngModel)]=\"userData.displayName\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">First Name</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"firstName\" placeholder=\"First Name\" [(ngModel)]=\"userData.fname\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Last Name</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"lastName\" placeholder=\"Last Name\" [(ngModel)]=\"userData.lname\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Email</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"email\" placeholder=\"Email\" [(ngModel)]=\"userData.email\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Phone number</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"department\" placeholder=\"Phone number\" [(ngModel)]=\"userData.phoneNumber\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Birth Date</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"date\" name=\"title\" placeholder=\"Birth date\" [(ngModel)]=\"userData.birthday\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Address</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"startDate\" placeholder=\"Address\" [(ngModel)]=\"userData.address\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">City</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"location\" placeholder=\"City\" [(ngModel)]=\"userData.city\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">State</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"State\" [(ngModel)]=\"userData.state\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Country</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"Country\" [(ngModel)]=\"userData.country\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Zip</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"Zip code\" [(ngModel)]=\"userData.zip\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <label class=\"col-sm-4 control-label\">Country code</label>\n                                                    <div class=\"col-sm-4\">\n                                                        <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"Country code (+91)\" [(ngModel)]=\"userData.countryCode\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"form-group row\">\n                                                    <div class=\"col-sm-offset-2 col-sm-10\">\n                                                        <button class=\"btn btn-primary\" (click)=\"updateMe(userData)\">Update</button>\n                                                    </div>\n                                                </div>\n\n                                            </form>\n                                        </div>\n                                        <!-- end form-horizontal -->\n                                    </div>\n                                    <!-- end panel-body -->\n\n                                </div>\n                                <!-- end panel -->\n\n\n                            </div>\n                            <!-- end size -->\n                        </div>\n                        <!-- end container-fluid -->\n\n\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n\n\n\n<div class=\"modal fade\" id=\"addContactModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"addContactModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"addContactModalLabel\">Add new contact</h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n                <form>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Profile Url</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"displayName\" placeholder=\"Profile Url\" [(ngModel)]=\"newUserData.avatar\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Display Name*</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"displayName\" placeholder=\"Display name\" [(ngModel)]=\"newUserData.displayName\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">First Name*</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"firstName\" placeholder=\"First Name\" [(ngModel)]=\"newUserData.fname\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Last Name*</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"lastName\" placeholder=\"Last Name\" [(ngModel)]=\"newUserData.lname\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Email*</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"email\" placeholder=\"Email\" [(ngModel)]=\"newUserData.email\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Phone number*</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"department\" placeholder=\"Phone number\" [(ngModel)]=\"newUserData.phoneNumber\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Birth Date</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"date\" name=\"title\" placeholder=\"Birth date\" [(ngModel)]=\"newUserData.birthday\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Address</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"startDate\" placeholder=\"Address\" [(ngModel)]=\"newUserData.address\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">City</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"location\" placeholder=\"City\" [(ngModel)]=\"newUserData.city\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">State</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"State\" [(ngModel)]=\"newUserData.state\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Country</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"Country\" [(ngModel)]=\"newUserData.country\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Zip</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"Zip code\" [(ngModel)]=\"newUserData.zip\">\n                        </div>\n                    </div>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Country code</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"phone\" placeholder=\"Country code (+91)\" [(ngModel)]=\"newUserData.countryCode\">\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" id=\"closeModal\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n                <button type=\"button\" class=\"btn btn-primary\" (click)=\"addContact()\">Save</button>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<div class=\"modal fade\" id=\"addProfileModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"addContactModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"addContactModalLabel\">Add profile url</h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n                <form>\n                    <div class=\"form-group row\">\n                        <label class=\"col-sm-4 control-label\">Profile Url</label>\n                        <div class=\"col-sm-8\">\n                            <input class=\"form-control\" type=\"text\" name=\"displayName\" placeholder=\"Profile Url\" [(ngModel)]=\"userData.avatar\">\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" id=\"closeModal\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n                <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Save</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/component/landing/landing.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/component/landing/landing.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"container\">\n    <h1>Welcome to CMS</h1>\n    <p>\n        CMS ia a simple web app for contact management where you can save and manage your contacts.\n    </p>\n    <p>Made with <i class=\"fa fa-heart\"></i> by Prasad Pawar</p>\n    <a [routerLink]=\"['home']\" class=\"button\">Get started</a>\n</div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _component_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./component/landing/landing.component */ "./src/app/component/landing/landing.component.ts");
/* harmony import */ var _component_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./component/home/home.component */ "./src/app/component/home/home.component.ts");





var routes = [
    { path: "", component: _component_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__["LandingComponent"] },
    { path: "home", component: _component_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'cms1';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _component_landing_landing_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./component/landing/landing.component */ "./src/app/component/landing/landing.component.ts");
/* harmony import */ var _component_home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./component/home/home.component */ "./src/app/component/home/home.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _component_landing_landing_component__WEBPACK_IMPORTED_MODULE_5__["LandingComponent"],
                _component_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/component/home/home.component.css":
/*!***************************************************!*\
  !*** ./src/app/component/home/home.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n    margin: 0;\n    padding: 0;\n    background-color: #d9dbd4;\n}\n\n.wcontainer {\n    height: 80vh;\n}\n\n.whatsapp {\n    background: #009688;\n    width: 100%;\n    height: 100px;\n    padding: 30px;\n}\n\n.whatsapp_main {\n    width: 90%;\n    background-color: white;\n    height: 450px;\n    margin: 0px auto 30px auto;\n    position: absolute;\n    top: 5%;\n    left: 0;\n    right: 0;\n}\n\n.whatsapp_main_sidemenu {\n    background-color: #eee;\n    height: 100%;\n    width: 100%;\n}\n\n.whatsapp_main_sidemenu_top .row .avatar {\n    height: 70px;\n    width: 70px;\n}\n\n.whatsapp_main_sidemenu_top .row .avatar img {\n    height: 62px;\n    width: 62px;\n}\n\n.whatsapp_main_sidemenu_middle {\n    background-color: #ada4a4;\n    padding: 10px 16px;\n}\n\n.whatsapp_main_sidemenu_middle i.fa-search {\n    position: absolute;\n    left: 41px;\n    -webkit-transform: translate(0%, 100%);\n            transform: translate(0%, 100%);\n    font-size: 19px;\n    color: gray;\n}\n\n.whatsapp_main_sidemenu_middle input {\n    width: 100%;\n    border: none;\n    border-radius: 50px;\n    padding: 5px 30px;\n}\n\n.whatsapp_main_sidemenu_middle input:focus {\n    outline: none;\n}\n\n.scroll {\n    height: 600px !important;\n    overflow-y: auto !important;\n}\n\n.whatsapp_main_sidemenu_bottom {\n    background-color: #fff;\n    height: 500px !important;\n    overflow-y: auto !important;\n}\n\n.whatsapp_main_sidemenu_bottom_chat {\n    overflow-y: scroll;\n    height: 500px !important;\n}\n\n.whatsapp_main_body {\n    background-color: #f7f9fa;\n    width: 100%;\n    height: 100%;\n    margin: 0 -15px;\n    text-align: center;\n}\n\n.whatsapp_main_body_image {\n    text-align: center;\n}\n\n.whatsapp_main_body_image img {\n    padding-top: 50px;\n}\n\n.avatar {\n    height: 70px;\n    width: 70px;\n}\n\n@media only screen and (max-device-width: 480px) {\n    /* For mobile phones: */\n    #searchBox {\n        width: 225px !important;\n    }\n    .centered {\n        margin-left: 0%;\n    }\n    .panel {\n        overflow: auto\n    }\n}\n\n.centered {\n    margin-left: 30%;\n}\n\nlabel {\n    font-size: 20px;\n}\n\n.highlight {\n    background-color: cyan;\n    font-weight: bold;\n}\n\nli:active {\n    background: grey;\n}\n\nli:focus {\n    background: grey;\n}\n\n.hovereffect {\n    float: left;\n    overflow: hidden;\n    position: relative;\n    text-align: center;\n    cursor: default;\n}\n\n.hovereffect .overlay {\n    width: 100%;\n    height: 100%;\n    position: absolute;\n    overflow: hidden;\n    top: 0;\n    left: 0;\n    opacity: 0;\n    background-color: rgba(0, 0, 0, 0.5);\n    -webkit-transition: all .4s ease-in-out;\n    transition: all .4s ease-in-out\n}\n\n.hovereffect img {\n    display: block;\n    position: relative;\n    -webkit-transition: all .4s linear;\n    transition: all .4s linear;\n}\n\n.hovereffect h2 {\n    text-transform: uppercase;\n    color: #fff;\n    text-align: center;\n    position: relative;\n    font-size: 17px;\n    background: rgba(0, 0, 0, 0.6);\n    -webkit-transform: translatey(-100px);\n    transform: translatey(-100px);\n    -webkit-transition: all .2s ease-in-out;\n    transition: all .2s ease-in-out;\n    padding: 10px;\n}\n\n.hovereffect a.info {\n    text-decoration: none;\n    display: inline-block;\n    text-transform: uppercase;\n    color: #fff;\n    border: 1px solid #fff;\n    background-color: transparent;\n    opacity: 0;\n    filter: alpha(opacity=0);\n    -webkit-transition: all .2s ease-in-out;\n    transition: all .2s ease-in-out;\n}\n\n.hovereffect a.info:hover {\n    box-shadow: 0 0 5px #fff;\n}\n\n.hovereffect:hover img {\n    -webkit-transform: scale(1.2);\n    transform: scale(1.2);\n}\n\n.hovereffect:hover .overlay {\n    opacity: 1;\n    filter: alpha(opacity=100);\n}\n\n.hovereffect:hover h2,\n.hovereffect:hover a.info {\n    opacity: 1;\n    filter: alpha(opacity=100);\n    -webkit-transform: translatey(0);\n    transform: translatey(0);\n}\n\n.hovereffect:hover a.info {\n    -webkit-transition-delay: .2s;\n    transition-delay: .2s;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksU0FBUztJQUNULFVBQVU7SUFDVix5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxhQUFhO0lBQ2IsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLFVBQVU7SUFDVix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLE9BQU87SUFDUCxRQUFRO0FBQ1o7O0FBRUE7SUFDSSxzQkFBc0I7SUFDdEIsWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFlBQVk7SUFDWixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxZQUFZO0lBQ1osV0FBVztBQUNmOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1Ysc0NBQThCO1lBQTlCLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsV0FBVztBQUNmOztBQUVBO0lBQ0ksV0FBVztJQUNYLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLHdCQUF3QjtJQUN4QiwyQkFBMkI7QUFDL0I7O0FBRUE7SUFDSSxzQkFBc0I7SUFDdEIsd0JBQXdCO0lBQ3hCLDJCQUEyQjtBQUMvQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQix3QkFBd0I7QUFDNUI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsV0FBVztJQUNYLFlBQVk7SUFDWixlQUFlO0lBQ2Ysa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLHVCQUF1QjtJQUN2QjtRQUNJLHVCQUF1QjtJQUMzQjtJQUNBO1FBQ0ksZUFBZTtJQUNuQjtJQUNBO1FBQ0k7SUFDSjtBQUNKOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLHNCQUFzQjtJQUN0QixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixNQUFNO0lBQ04sT0FBTztJQUNQLFVBQVU7SUFDVixvQ0FBb0M7SUFDcEMsdUNBQXVDO0lBQ3ZDO0FBQ0o7O0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGtDQUFrQztJQUNsQywwQkFBMEI7QUFDOUI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLDhCQUE4QjtJQUM5QixxQ0FBcUM7SUFFckMsNkJBQTZCO0lBQzdCLHVDQUF1QztJQUN2QywrQkFBK0I7SUFDL0IsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLFVBQVU7SUFDVix3QkFBd0I7SUFDeEIsdUNBQXVDO0lBQ3ZDLCtCQUErQjtBQUNuQzs7QUFFQTtJQUNJLHdCQUF3QjtBQUM1Qjs7QUFFQTtJQUVJLDZCQUE2QjtJQUM3QixxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsMEJBQTBCO0FBQzlCOztBQUVBOztJQUVJLFVBQVU7SUFDViwwQkFBMEI7SUFFMUIsZ0NBQWdDO0lBQ2hDLHdCQUF3QjtBQUM1Qjs7QUFFQTtJQUNJLDZCQUE2QjtJQUM3QixxQkFBcUI7QUFDekIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnQvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5IHtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDlkYmQ0O1xufVxuXG4ud2NvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiA4MHZoO1xufVxuXG4ud2hhdHNhcHAge1xuICAgIGJhY2tncm91bmQ6ICMwMDk2ODg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBwYWRkaW5nOiAzMHB4O1xufVxuXG4ud2hhdHNhcHBfbWFpbiB7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBoZWlnaHQ6IDQ1MHB4O1xuICAgIG1hcmdpbjogMHB4IGF1dG8gMzBweCBhdXRvO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDUlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG59XG5cbi53aGF0c2FwcF9tYWluX3NpZGVtZW51IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLndoYXRzYXBwX21haW5fc2lkZW1lbnVfdG9wIC5yb3cgLmF2YXRhciB7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIHdpZHRoOiA3MHB4O1xufVxuXG4ud2hhdHNhcHBfbWFpbl9zaWRlbWVudV90b3AgLnJvdyAuYXZhdGFyIGltZyB7XG4gICAgaGVpZ2h0OiA2MnB4O1xuICAgIHdpZHRoOiA2MnB4O1xufVxuXG4ud2hhdHNhcHBfbWFpbl9zaWRlbWVudV9taWRkbGUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNhZGE0YTQ7XG4gICAgcGFkZGluZzogMTBweCAxNnB4O1xufVxuXG4ud2hhdHNhcHBfbWFpbl9zaWRlbWVudV9taWRkbGUgaS5mYS1zZWFyY2gge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA0MXB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAlLCAxMDAlKTtcbiAgICBmb250LXNpemU6IDE5cHg7XG4gICAgY29sb3I6IGdyYXk7XG59XG5cbi53aGF0c2FwcF9tYWluX3NpZGVtZW51X21pZGRsZSBpbnB1dCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgcGFkZGluZzogNXB4IDMwcHg7XG59XG5cbi53aGF0c2FwcF9tYWluX3NpZGVtZW51X21pZGRsZSBpbnB1dDpmb2N1cyB7XG4gICAgb3V0bGluZTogbm9uZTtcbn1cblxuLnNjcm9sbCB7XG4gICAgaGVpZ2h0OiA2MDBweCAhaW1wb3J0YW50O1xuICAgIG92ZXJmbG93LXk6IGF1dG8gIWltcG9ydGFudDtcbn1cblxuLndoYXRzYXBwX21haW5fc2lkZW1lbnVfYm90dG9tIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIGhlaWdodDogNTAwcHggIWltcG9ydGFudDtcbiAgICBvdmVyZmxvdy15OiBhdXRvICFpbXBvcnRhbnQ7XG59XG5cbi53aGF0c2FwcF9tYWluX3NpZGVtZW51X2JvdHRvbV9jaGF0IHtcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gICAgaGVpZ2h0OiA1MDBweCAhaW1wb3J0YW50O1xufVxuXG4ud2hhdHNhcHBfbWFpbl9ib2R5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmOWZhO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBtYXJnaW46IDAgLTE1cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ud2hhdHNhcHBfbWFpbl9ib2R5X2ltYWdlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi53aGF0c2FwcF9tYWluX2JvZHlfaW1hZ2UgaW1nIHtcbiAgICBwYWRkaW5nLXRvcDogNTBweDtcbn1cblxuLmF2YXRhciB7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIHdpZHRoOiA3MHB4O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtZGV2aWNlLXdpZHRoOiA0ODBweCkge1xuICAgIC8qIEZvciBtb2JpbGUgcGhvbmVzOiAqL1xuICAgICNzZWFyY2hCb3gge1xuICAgICAgICB3aWR0aDogMjI1cHggIWltcG9ydGFudDtcbiAgICB9XG4gICAgLmNlbnRlcmVkIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAlO1xuICAgIH1cbiAgICAucGFuZWwge1xuICAgICAgICBvdmVyZmxvdzogYXV0b1xuICAgIH1cbn1cblxuLmNlbnRlcmVkIHtcbiAgICBtYXJnaW4tbGVmdDogMzAlO1xufVxuXG5sYWJlbCB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4uaGlnaGxpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBjeWFuO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG5saTphY3RpdmUge1xuICAgIGJhY2tncm91bmQ6IGdyZXk7XG59XG5cbmxpOmZvY3VzIHtcbiAgICBiYWNrZ3JvdW5kOiBncmV5O1xufVxuXG4uaG92ZXJlZmZlY3Qge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjdXJzb3I6IGRlZmF1bHQ7XG59XG5cbi5ob3ZlcmVmZmVjdCAub3ZlcmxheSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIG9wYWNpdHk6IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC40cyBlYXNlLWluLW91dDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjRzIGVhc2UtaW4tb3V0XG59XG5cbi5ob3ZlcmVmZmVjdCBpbWcge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuNHMgbGluZWFyO1xuICAgIHRyYW5zaXRpb246IGFsbCAuNHMgbGluZWFyO1xufVxuXG4uaG92ZXJlZmZlY3QgaDIge1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjYpO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGV5KC0xMDBweCk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRleSgtMTAwcHgpO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRleSgtMTAwcHgpO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2UtaW4tb3V0O1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5ob3ZlcmVmZmVjdCBhLmluZm8ge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIG9wYWNpdHk6IDA7XG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTApO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2UtaW4tb3V0O1xufVxuXG4uaG92ZXJlZmZlY3QgYS5pbmZvOmhvdmVyIHtcbiAgICBib3gtc2hhZG93OiAwIDAgNXB4ICNmZmY7XG59XG5cbi5ob3ZlcmVmZmVjdDpob3ZlciBpbWcge1xuICAgIC1tcy10cmFuc2Zvcm06IHNjYWxlKDEuMik7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEuMik7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjIpO1xufVxuXG4uaG92ZXJlZmZlY3Q6aG92ZXIgLm92ZXJsYXkge1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTEwMCk7XG59XG5cbi5ob3ZlcmVmZmVjdDpob3ZlciBoMixcbi5ob3ZlcmVmZmVjdDpob3ZlciBhLmluZm8ge1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTEwMCk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRleSgwKTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRleSgwKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZXkoMCk7XG59XG5cbi5ob3ZlcmVmZmVjdDpob3ZlciBhLmluZm8ge1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogLjJzO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IC4ycztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/component/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/component/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_dal_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/dal.service */ "./src/app/services/dal.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "../node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);





var HomeComponent = /** @class */ (function () {
    function HomeComponent(dal) {
        this.dal = dal;
        this.userData = {
            avatar: "",
            birthday: Date,
            phoneNumber: null,
            email: "",
            country: "",
            countryCode: "",
            displayName: "",
            fname: "",
            lname: "",
            address: "",
            city: "",
            state: "",
            zip: "94920"
        };
        this.newUserData = {
            id: Math.floor(Math.random() * 90 + 10),
            avatar: "",
            birthday: Date,
            phoneNumber: null,
            email: "",
            country: "",
            countryCode: "",
            displayName: "",
            fname: "",
            lname: "",
            address: "",
            city: "",
            state: "",
            zip: "94920"
        };
    }
    HomeComponent.prototype.ngOnInit = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4___default()(document).ready(function () {
            jquery__WEBPACK_IMPORTED_MODULE_4___default()("#searchBox").on("keyup", function () {
                var value = jquery__WEBPACK_IMPORTED_MODULE_4___default()(this).val().toLowerCase();
                jquery__WEBPACK_IMPORTED_MODULE_4___default()("#myList li").filter(function () {
                    jquery__WEBPACK_IMPORTED_MODULE_4___default()(this).toggle(jquery__WEBPACK_IMPORTED_MODULE_4___default()(this).text().toLowerCase().indexOf(value) > -1);
                });
            });
            jquery__WEBPACK_IMPORTED_MODULE_4___default()('.contactList').click(function (e) {
                console.log("wwwwww", e);
                e.preventDefault();
                jquery__WEBPACK_IMPORTED_MODULE_4___default()(this).css('background-color', 'red');
            });
        });
        console.log("userdata", this.userData);
        this.getContactList();
    };
    HomeComponent.prototype.getContactList = function () {
        var _this = this;
        this.dal.getContactList().subscribe(function (x) {
            _this.contactList = x;
            console.log("contact list fetched", x);
        });
    };
    HomeComponent.prototype.getUserDetailsById = function (id) {
        this.detailsById = this.contactList.filter(function (x) { return x.id == id; });
        console.log("details by id", this.detailsById);
        this.userData = this.detailsById[0];
    };
    HomeComponent.prototype.updateMe = function (data) {
        var _this = this;
        if (!this.validateUpdateContact())
            return;
        //console.log("update data", data)
        this.contactList.map(function (obj) { return [data].find(function (o) { return o.id === obj.id; }) || obj; });
        // console.log("updated contact list", this.contactList);
        this.dal.updateContact(this.contactList).subscribe(function (x) {
            console.log("update contact res", x);
            _this.getContactList();
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'success',
                title: "Success!",
                text: "User Information updated successfully!",
                timer: 3000
            });
        });
    };
    HomeComponent.prototype.isContactExists = function (phoneNumber, arr) {
        return arr.some(function (el) {
            return el.phoneNumber == phoneNumber;
        });
    };
    HomeComponent.prototype.addContact = function () {
        var _this = this;
        console.log(this.validateAddContact());
        if (!this.validateAddContact())
            return;
        if (this.isContactExists(this.newUserData.phoneNumber, this.contactList)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: "Error!",
                text: "User already registered with this phone number!",
                timer: 3000
            });
        }
        else {
            this.contactList.push(this.newUserData);
            console.log("after adding", this.contactList);
            //let prev = this.contactList.filter(x => x.phoneNumber == this.newUserData.phoneNumber)
            this.dal.updateContact(this.contactList).subscribe(function (x) {
                console.log("after adding contact", x);
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    icon: 'success',
                    title: "Success!",
                    text: "User Information added successfully!",
                    timer: 3000
                });
                _this.getContactList();
            });
            document.getElementById('closeModal').click();
        }
    };
    HomeComponent.prototype.validateAddContact = function () {
        if (this.newUserData.displayName == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter display name!',
                timer: 3000
            });
            return false;
        }
        if (this.newUserData.fname == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter first name!',
                timer: 3000
            });
            return false;
        }
        if (this.newUserData.lname == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter last name!',
                timer: 3000
            });
            return false;
        }
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.newUserData.email)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter valid email!',
                timer: 3000
            });
            return false;
        }
        if (!/^\d{10}$/.test(this.newUserData.phoneNumber)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter valid mobile number!',
                timer: 3000
            });
            return false;
        }
        return true;
    };
    HomeComponent.prototype.validateUpdateContact = function () {
        if (this.userData.displayName == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter display name!',
                timer: 3000
            });
            return false;
        }
        if (this.userData.fname == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter first name!',
                timer: 3000
            });
            return false;
        }
        if (this.userData.lname == "") {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter last name!',
                timer: 3000
            });
            return false;
        }
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.userData.email)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter valid email!',
                timer: 3000
            });
            return false;
        }
        if (!/^\d{10}$/.test(this.userData.phoneNumber)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Enter valid mobile number!',
                timer: 3000
            });
            return false;
        }
        return true;
    };
    HomeComponent.ctorParameters = function () { return [
        { type: src_app_services_dal_service__WEBPACK_IMPORTED_MODULE_2__["DalService"] }
    ]; };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/component/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/component/home/home.component.css")]
        })
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/component/landing/landing.component.css":
/*!*********************************************************!*\
  !*** ./src/app/component/landing/landing.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n    margin: 0;\n    padding: 0;\n}\n\nbody {\n    margin: 0;\n    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;\n    font-size: 20px;\n    color: black;\n    line-height: 1.9;\n}\n\n#container {\n    background-image: url('https://images.pexels.com/photos/373076/pexels-photo-373076.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260');\n    background-size: cover;\n    background-position: center;\n    height: 100vh;\n    display: -webkit-box;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n            flex-direction: column;\n    -webkit-box-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n            align-items: center;\n    text-align: center;\n    padding: 0 20px;\n}\n\n#container h1 {\n    font-size: 50px;\n    line-height: 1.2;\n}\n\n#container p {\n    font-size: 20px;\n}\n\n#container .button {\n    font-size: 18px;\n    text-decoration: none;\n    color: black;\n    border: 1px solid black;\n    padding: 10px 20px;\n    border-radius: 10px;\n    margin-top: 20px;\n}\n\n#container .button:hover {\n    background: black;\n    color: blanchedalmond;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50L2xhbmRpbmcvbGFuZGluZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksU0FBUztJQUNULFVBQVU7QUFDZDs7QUFFQTtJQUNJLFNBQVM7SUFDVCx3RUFBd0U7SUFDeEUsZUFBZTtJQUNmLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxzSUFBc0k7SUFDdEksc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQixhQUFhO0lBQ2Isb0JBQWE7SUFBYixhQUFhO0lBQ2IsNEJBQXNCO0lBQXRCLDZCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2Qix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIscUJBQXFCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50L2xhbmRpbmcvbGFuZGluZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKiB7XG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbmJvZHkge1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LWZhbWlseTogJ0ZyYW5rbGluIEdvdGhpYyBNZWRpdW0nLCAnQXJpYWwgTmFycm93JywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBsaW5lLWhlaWdodDogMS45O1xufVxuXG4jY29udGFpbmVyIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vaW1hZ2VzLnBleGVscy5jb20vcGhvdG9zLzM3MzA3Ni9wZXhlbHMtcGhvdG8tMzczMDc2LmpwZWc/YXV0bz1jb21wcmVzcyZjcz10aW55c3JnYiZkcHI9MyZoPTc1MCZ3PTEyNjAnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwMHZoO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG59XG5cbiNjb250YWluZXIgaDEge1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICBsaW5lLWhlaWdodDogMS4yO1xufVxuXG4jY29udGFpbmVyIHAge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuI2NvbnRhaW5lciAuYnV0dG9uIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4jY29udGFpbmVyIC5idXR0b246aG92ZXIge1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/component/landing/landing.component.ts":
/*!********************************************************!*\
  !*** ./src/app/component/landing/landing.component.ts ***!
  \********************************************************/
/*! exports provided: LandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingComponent", function() { return LandingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LandingComponent = /** @class */ (function () {
    function LandingComponent() {
    }
    LandingComponent.prototype.ngOnInit = function () {
    };
    LandingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing',
            template: __webpack_require__(/*! raw-loader!./landing.component.html */ "./node_modules/raw-loader/index.js!./src/app/component/landing/landing.component.html"),
            styles: [__webpack_require__(/*! ./landing.component.css */ "./src/app/component/landing/landing.component.css")]
        })
    ], LandingComponent);
    return LandingComponent;
}());



/***/ }),

/***/ "./src/app/services/custom-http.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/custom-http.service.ts ***!
  \*************************************************/
/*! exports provided: CustomHttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomHttpService", function() { return CustomHttpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CustomHttpService = /** @class */ (function () {
    function CustomHttpService(http) {
        this.http = http;
    }
    CustomHttpService.prototype.get = function (url) {
        return this.http.get(url);
    };
    CustomHttpService.prototype.post = function (url, payload) {
        // let headers = new Headers();
        // headers.append('Access-Control-Allow-Origin', 'https:://jsonblob.com');
        // headers.append('Access-Control-Allow-Methods', 'POST, PUT, DELETE, HEAD, PATCH, OPTIONS');
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Access-Control-Allow-Origin': 'https:://jsonblob.com',
            'Access-Control-Allow-Methods': 'POST, PUT, DELETE, HEAD, PATCH, OPTIONS'
        });
        var options = { headers: headers };
        //let options = new RequestOptions({ headers: headers });
        return this.http.post(url, payload, options);
    };
    CustomHttpService.prototype.put = function (url, payload) {
        return this.http.put(url, payload);
    };
    CustomHttpService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    CustomHttpService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], CustomHttpService);
    return CustomHttpService;
}());



/***/ }),

/***/ "./src/app/services/dal.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/dal.service.ts ***!
  \*****************************************/
/*! exports provided: DalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DalService", function() { return DalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _custom_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom-http.service */ "./src/app/services/custom-http.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




var DalService = /** @class */ (function () {
    function DalService(customHttp) {
        this.customHttp = customHttp;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url;
    }
    DalService.prototype.getContactList = function () {
        return this.customHttp.get(this.url);
    };
    DalService.prototype.addContact = function (payload) {
        return this.customHttp.post(this.url, payload);
    };
    DalService.prototype.updateContact = function (payload) {
        return this.customHttp.put(this.url, payload);
    };
    DalService.ctorParameters = function () { return [
        { type: _custom_http_service__WEBPACK_IMPORTED_MODULE_2__["CustomHttpService"] }
    ]; };
    DalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], DalService);
    return DalService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    url: "https://jsonblob.com/api/jsonBlob/3fa59102-63ca-11ea-ad21-090aef4d4a62"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/bhushan/data/prasad/angular-projects/intouchapp/cms/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map